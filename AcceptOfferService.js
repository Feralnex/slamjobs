//Load modules
const { ServiceBroker } = require("moleculer");
var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";
var mongo = require("mongodb");

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
    nodeID: "AcceptService",
    transporter: "NATS"
});

broker.createService({
    name: "accept",
    actions: {
      async do(ctx) {

        var user_id = ctx.params.user_id;
        var offer_id = ctx.params.offer_id;

        let client = await MongoClient.connect(url);
        let db = client.db("slamjobs");
        let collection = db.collection("Form");

        await collection.findOneAndUpdate({ _id: offer_id },{toUser : user_id},function(err, obj) {
          if (err) throw err;
        });
        client.close();
      }
    }
  });

  //Start server
broker.start();