//Load modules
const { ServiceBroker } = require("moleculer");
var async = require('async');
var crypto = require('crypto');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/users');

var User = require('./models/user');

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
  nodeID: "DeleteUserService",
  transporter: "NATS"
});

broker.createService({
  name: "delete",
  actions: {
    prepare(ctx) {
      var email = ctx.params.email;

      async.waterfall([
        function (done) {
          crypto.randomBytes(20, function (err, buf) {
            var token = buf.toString('hex');
            done(err, token);
          });
        },
        function (token, done) {
          User.findOne({ email: email }, function (err, user) {

            user.deleteVerificationToken = token;

            user.save(function (err) {
              done(err, token, user);
            });
          });
        },
        function (token, user, done) {
          broker.call('mail.deleteVerification', { token: token, user: user, done: done })
        }
      ], function (err) {
        if (err) return next(err);
        return;
      });
    },
    confirm(ctx){
      var token = ctx.params.token;

      User.findOneAndDelete({ deleteVerificationToken: token }, function (err, user) {
        if (!user) {
            flash('error', 'Account doesn\'t exist anymore');
            return res.redirect('/');
        }
      });
    }
  }
});

//Start server
broker.start();