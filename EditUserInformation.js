//Load modules
const { ServiceBroker } = require("moleculer");
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/users');

var User = require('./models/user');

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
  nodeID: "EditUserInformationService",
  transporter: "NATS"
});

broker.createService({
  name: "edit",
  actions: {
    user(ctx) {

      var email = ctx.params.email;
      var firstName = ctx.params.firstName;
      var lastName = ctx.params.lastName;
      var companyName = ctx.params.companyName;
      var companyLocation = ctx.params.companyLocation;

      User.findOne({ email: email }, function (err, user) {
        if (!user) {
          flash('error', 'No account with that email address exists.');
          return res.redirect('/');
        }

        user.firstName = firstName;
        user.lastName = lastName;
        user.companyName = companyName;
        user.companyLocation = companyLocation;

        user.save(function (err) {
          if (err) {
            flash('error', 'Error during user information update');
          }
        });
      });
    }
  }
});

//Start server
broker.start();