//Load modules
const { ServiceBroker } = require("moleculer");
var mongo = require("mongodb");
var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
	nodeID: "ListingService",
	transporter: "NATS"
});

//Create service with actions on requests
broker.createService({
	name: "offers",
	actions: {
		async get(ctx) {
			let client = await MongoClient.connect(url);
			let db = client.db("slamjobs");
			let collection = db.collection("Form");
			let result = "";
			if (ctx.params.id != null) {
				var o_id = new mongo.ObjectID(ctx.params.id);
				result = JSON.stringify(await collection.findOne({"_id": o_id}));
			} else if ((ctx.params.keyword != null) || (ctx.params.location != null)) {
				var keyword="";
				var location="";
				if (ctx.params.keyword != null) {
					keyword = ctx.params.keyword;
				}
				if (ctx.params.location != null) {
					location = ctx.params.location;
				}
				console.log(await collection.find(await collection.find({$and: [{"offerCategory": {$regex: ".*"+keyword+".*"}}, {"location": {$regex: ".*"+location+".*"}}]})));
				result = JSON.stringify(await collection.find({$and: [{"offerCategory": {$regex: ".*"+keyword+".*"}}, {"location": {$regex: ".*"+location+".*"}}]}).toArray());

			} else if (ctx.params.user_id != null) {
				var query = { "toUser": ctx.params.user_id };
				result = JSON.stringify(await collection.find(query)).toArray();
			} else {
				result = JSON.stringify(await collection.find({}).toArray());
			}
			await client.close();
			return JSON.parse(result);
		}
	}
});

//Start server
broker.start();
