//Load modules
const { ServiceBroker } = require("moleculer");
const nodemailer = require('nodemailer');

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
    nodeID: "MailService",
    transporter: "NATS"
});

//Create service with mailing functions
broker.createService({
    name: "mail",
    actions: {
        sendToken(ctx) {
            var token = ctx.params.token;
            var user = ctx.params.user;
            var done = ctx.params.done;
            var flash = ctx.params.flash;

            var smtpTransport = nodemailer.createTransport({
                service: 'SendGrid',
                auth: {
                    user: 'slamjobs',
                    pass: 'rampagers666'
                }
            });
            console.log(user.email);
            var mailOptions = {
                to: user.email,
                from: 'passwordreset@slamjobs.com',
                subject: 'Node.js Password Reset',
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                    'http://' + 'localhost:3000' + '/reset/' + token + '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                done(err, 'done');
            });
        },
        passwordReset(ctx) {
            var user = ctx.params.user;
            var done = ctx.params.done;

            var smtpTransport = nodemailer.createTransport({
                service: 'SendGrid',
                auth: {
                    user: 'slamjobs',
                    pass: 'rampagers666'
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'passwordreset@slamjobs.com',
                subject: 'Your password has been changed',
                text: 'Hello,\n\n' +
                    'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                req.flash('success', 'Success! E-mail has been sent.');
                done(err);
            });
        },
        accountVerification(ctx) {
            var user = ctx.params.user;
            var token = ctx.params.token;

            var smtpTransport = nodemailer.createTransport({
                service: 'SendGrid',
                auth: {
                    user: 'slamjobs',
                    pass: 'rampagers666'
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'accountVerification@slamjobs.com',
                subject: 'Activate your account',
                text: 'Hello,\n\n' +
                    'This is a confirmation that the account:' + user.emai + 
                    ' has been created. Please click on the following link, or paste this into your browser to complete the activation:\n' +
                    'http://' + 'localhost:3000' + '/activate/' + token
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                req.flash('success', 'Success! E-mail has been sent.');
                done(err);
            });
        },
        deleteVerification(ctx) {
            var user = ctx.params.user;
            var token = ctx.params.token;

            var smtpTransport = nodemailer.createTransport({
                service: 'SendGrid',
                auth: {
                    user: 'slamjobs',
                    pass: 'rampagers666'
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'deleteVerification@slamjobs.com',
                subject: 'Deleting account',
                text: 'Hello,\n\n' +
                    'This is a confirmation that the account: ' + user.email + 
                    ' is going to be deleted. Please click on the following link, or paste this into your browser to complete the process:\n' +
                    'http://' + 'localhost:3000' + '/delete/' + token
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                req.flash('success', 'Success! E-mail has been sent.');
                done(err);
            });
        }
    }
});

//Start server
broker.start();