const { ServiceBroker } = require("moleculer");
const express = require("express");
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/slamjobs');

var Offer = require('./models/form');

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
  nodeID: "OfferService",
  transporter: "NATS"
});

broker.createService({
    name: "Offer",
    actions: {
      new(ctx) {

        var title = ctx.params.title;
        var language = ctx.params.language;
        var description = ctx.params.description;
        var location = ctx.params.location;
        var minSalary = ctx.params.minSalary;
        var maxSalary = ctx.params.maxSalary;
        var job = ctx.params.job;
        var technology = ctx.params.technology;
        var email = ctx.params.email;
        var company = ctx.params.company;
        if (job=="work Order") var isWork=true;
        else var isWork=false;
        var newOffer = new Offer({
          
            title: title,
            offerCategory: title,
            location: location,
            minSalary: minSalary,
            maxSalary: maxSalary,
            description: description,
            language: language,
            technology: technology,
            date: Date.now(),            
            userEmail: email,
            company:company,
            toUser:"",
            isWork: isWork

        });
        console.log(newOffer);
        newOffer.save(function (err) {
          if (err) return handleError(err);
        });
      }
    }
  });


//Start server
broker.start();