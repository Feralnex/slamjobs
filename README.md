# Slam$Jobs
Slam$Jobs is a Web Application project which allows users (such as companies and freelancers) to post their job offers.
## Getting Started
Application is based on Microservice Architecture. Each server offers max 2 services. Our application consists of 5 layers:
* First layer = Presentation layer
* Second layer contains Web Service
* Third layer contains Message Broker
* Fourth layer constains microservices
* Fifth layer contains Database

Web Service sends requests to Mesasage Broker based on user action.
Message Broker is connected to all microservices and is responsible for choosing accurate service for satysfying response and handling connections between microservices.
Microservices are located on 5 different servers. Each server performs max 2 actions.
Database contain information about users and their job offers.

### Getting Offers
Depending on user choice (work orders or job offers) the request is made by Web Service to Message Broker. Message Broker receives request and transfers it to accurate microservice. Microservice receives request and based on it requests specific data from Database. After getting response from Database (list of offers) the response for Message Broker is made to be delivered to Web Service which generates the Web Page for user.
### Creating New Account
To post offer user has to have an account. If he doesn't have any then he needs to create one. The process starts with clicking "Register" then "As User" or "As Company". The next step is to fill form and click "Confirm". The request to Message Broker is send. The process is similar to "Getting Offers" with a significant difference: Microservice sends request to add user to Database. The response from Database is going back to user through the same route with information about potential errors or successful registration. If it ended successfully the request to send mail is transfered from Microservice to another Microservice through Message Broker. After that user will get a mail in order to authenticate the e-mail address. After clicking link in the mail the next request from Web Service to Microservice is made which updates user record in Database that e-mail is verified.
### Deleting Account
Web Application offers every user to delete his account. The request goes from Web Service through Message Broker and Microservice to Database. If the process ended successfully the request to send mail is transfered by Microservice to another Microservice through Message Broker. The response is send to Web Service through the same route. After clicking link in the mail the next request from Web Service to Microservice is made which deletes user record in Database and all related offers.
### Creating Offer
User is able to create offer by clicking “Create Offer” where the user has to chose whether it will be job offer or freelancer offer. The offer can be created in two ways but the process for both of them is the same: First of all user fills up the form and clicks “Create”. In that moment Web Service sends request to Microservice through Message Broker. Microservice choses the right action depending on the type of the offer to save it in Database. The response goes back through the same route to Web Service and informs user if the process ended successfully.
### Deleting offer
User is able to delete his offer by selecting it in his “Offer List” and by clicking “Delete This Offer”. After that Web Service sends request to Microservice through Message Broker. Microservice choses the right action depending on the type of the offer to delete it from Database. The response goes back through the same route to Web Service and informs user if the process ended successfully.
### Accepting Offer
User is able to accept somebody’s offer by selecting offer from the list in the main page and by clicking “Accept Offer”. First of all user fills up the form and clicks “Send”. In that moment Web Service sends request through Message Broker to Microservice which sends mail to employer. The response goes back through the same route to Web Service and informs user if the process ended successfully.
### Sending Offer
User is able to send offer by selecting freelancer from the list in the main page and by clicking “Send Offer”. First of all user fills up the form and clicks “Send”. In that moment Web Service sends request through Message Broker to Microservice which sends mail to candidate. The response goes back through the same route to Web Service and informs user if the process ended successfully.
### Prerequisites
To run this project on your own machine you'll need:
* [Node.js](https://nodejs.org/en/)
* Code editor (e.g. [VS Code](https://code.visualstudio.com/))
* Database ([MongoDB](https://www.mongodb.com/) - our choice)

To run app on Node.js just type in your cmd:
```
$ node [path to your js app]
```