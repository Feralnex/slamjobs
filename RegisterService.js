//Load modules
const { ServiceBroker } = require("moleculer");
var crypto = require('crypto');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/users');

var User = require('./models/user');

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
  nodeID: "RegisterService",
  transporter: "NATS"
});

broker.createService({
  name: "register",
  actions: {
    do(ctx) {
      var email = ctx.params.email;
      var firstName = ctx.params.firstName;
      var lastName = ctx.params.lastName;
      var password = ctx.params.password;
      var companyName = ctx.params.companyName;
      var companyLocation = ctx.params.companyLocation;

      User.findOne({
        email: {
          "$regex": "^" + email + "\\b", "$options": "i"
        }
      }, function (err, mail) {
        if (mail) {
          //TODO print error
        }
        else {
          crypto.randomBytes(20, function (err, buf) {
            var token = buf.toString('hex');

            var newUser = new User({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password,
              companyLocation: companyLocation,
              companyName: companyName,
              accountVerificationToken: token
            });

            User.createUser(newUser, function (err, user) {
              if (err) throw err;
              console.log(user);
              broker.call('mail.accountVerification', { user: user, token: token });
            });
          });
        }
      });
    },
    activate(ctx) {
      var token = ctx.params.token;

      User.findOne({ accountVerificationToken: token }, function (err, user) {
        if (!user) {
            flash('error', 'Account doesn\'t exist anymore');
            return res.redirect('/');
        }

        user.active = true;
        user.accountVerificationToken = undefined;

        user.save(function (err) {
          if(err){
            throw err;
          }
      });
    });
    }
  }
});

//Start server
broker.start();