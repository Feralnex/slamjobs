//Load modules
const { ServiceBroker } = require("moleculer");
var async = require('async');
var crypto = require('crypto');
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/users');

User = require('./models/user');

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
    nodeID: "ResetPasswordService",
    transporter: "NATS"
});

//Create service with actions on requests
broker.createService({
    name: "reset",
    actions: {
        generateToken(ctx) {
            var email = ctx.params.email;
            var flash = ctx.params.flash;

            async.waterfall([
                function (done) {
                    crypto.randomBytes(20, function (err, buf) {
                        var token = buf.toString('hex');
                        done(err, token);
                    });
                },
                function (token, done) {
                    User.findOne({ email: email }, function (err, user) {
                        if (!user) {
                            flash('error', 'No account with that email address exists.');
                            return res.redirect('/forgot');
                        }

                        user.resetPasswordToken = token;
                        console.log(token);
                        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                        user.save(function (err) {
                            done(err, token, user);
                        });
                    });
                },
                function (token, user, done) {
                    broker.call('mail.sendToken', { token: token, user: user, done: done, flash: flash })
                }
            ], function (err) {
                if (err) return next(err);
                return;
            });
        },
        newPassword(ctx) {
            var password = ctx.params.password;
            var token = ctx.params.token;
            var localUser;

            async.waterfall([
                function (done) {
                    User.findOne({ resetPasswordToken: token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
                        if (!user) {
                            flash('error', 'Password reset token is invalid or has expired.');
                            return res.redirect('back');
                        }

                        user.password = password;
                        user.resetPasswordToken = undefined;
                        user.resetPasswordExpires = undefined;

                        bcrypt.genSalt(10, function (err, salt) {
                            bcrypt.hash(user.password, salt, function (err, hash) {
                                user.password = hash;

                                user.save(function (err) {
                                    localUser = user;
                                    done(err, user);
                                });
                            });
                        });
                    });
                },
                function (user, done) {
                    broker.call('mail.passwordReset', { user: user, done: done });
                }
            ], function (err) {
                res.redirect('/');
            });
            return localUser;
        }
    }
});

//Start server
broker.start();