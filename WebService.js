//Load modules
const { ServiceBroker } = require("moleculer");
const ApiService = require("moleculer-web");
const express = require("express");
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/users');

var User = require('./models/user');
var Form = require('./models/form');

passport.use(new LocalStrategy(
	function (email, password, done) {
		User.getUserByemail(email, function (err, user) {

			if (err) throw err;
			if (!user) {
				return done(null, false, { message: 'Unknown User' });
			}
			if (!user.active) {
				return done(null, false, { message: 'Account not verified' });
			}
			console.log(user);
			User.comparePassword(password, user.password, function (err, isMatch) {
				if (err) throw err;
				if (isMatch) {
					return done(null, user);
				} else {
					return done(null, false, { message: 'Invalid password' });
				}
			});
		});
	}));

passport.serializeUser(function (user, done) {
	done(null, user.id);
});

passport.deserializeUser(function (id, done) {
	User.getUserById(id, function (err, user) {
		done(err, user);
	});
});

//Create Express application
const app = express();

app.use(flash());

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({ defaultLayout: 'layout' }));
app.set('view engine', 'handlebars');

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Express Session
app.use(session({
	secret: 'secret',
	saveUninitialized: true,
	resave: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());
app.use(expressValidator());


// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

//Create Service Broker with transporter set to NATS
let broker = new ServiceBroker({
	nodeID: "APIGateway",
	transporter: "NATS"
});

// Express Validator
app.use(expressValidator({
	errorFormatter: function (param, msg, value) {
		var namespace = param.split('.')
			, root = namespace.shift()
			, formParam = root;

		while (namespace.length) {
			formParam += '[' + namespace.shift() + ']';
		}
		return {
			param: formParam,
			msg: msg,
			value: value
		};
	}
}));


//Load APIGateway as middleware
const svc = broker.createService({
	mixins: [ApiService],
	middleware: true,
});

// Global Vars
app.use(function (req, res, next) {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	res.locals.user = req.user || null;
	next();
});

//Use APIGateway as middleware for Express
app.use("/api", svc.express());

app.get('/', function (req, res) {
	broker.call("offers.get", {})
		.then(respond => {
			console.log(respond);
			res.render('index',  {
				offers: respond
			});
		});
});

app.post('/login',
	passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login', failureFlash: true }),
	function (req, res) {
	});

app.get('/login', function (req, res) {
	res.render('index', {
		invalidlogin: true
	});
});

app.post('/register', function (req, res) {

	var firstName = req.body.firstName;
	var lastName = req.body.lastName;
	var email = req.body.email;
	var email2 = req.body.confirmEmail;
	var password = req.body.passwrd;
	var password2 = req.body.confirmPassword;
	var companyName = req.body.companyName;
	var companyLocation = req.body.companyLocation;

	// Validation
	req.checkBody('firstName', 'First name is required').notEmpty();
	req.checkBody('lastName', 'Last name is required').notEmpty();
	req.checkBody('email', 'Email is required').notEmpty();
	req.checkBody('email', 'Email is not valid').isEmail();
	req.checkBody('email', 'Email is not valid').equals(email2);
	req.checkBody('passwrd', 'Password is required').notEmpty();
	req.checkBody('passwrd', 'Passwords do not match').equals(password2);

	var errors = req.validationErrors();

	if (errors) {
		console.log(errors);
		console.log("nie rejestruje");
		res.render('./index', {
			errors: errors
		});
	} else {
		broker.call("register.do", { firstName: firstName, lastName: lastName, email: email, password: password, companyName: companyName, companyLocation: companyLocation })
			.then(respond => res.render('index'), {
				successfulRegistration: true
			});;
	}
});

app.get("/offer/:id", function(req, res) {
	broker.call("offers.get", {id: req.params.id})
		.then(respond => {
			console.log(respond);
			res.render("index", {
				offer: respond
			});
		});
});

app.get("/offer/accepted/:user_id", function (req, res) {
	var u_id = mongo.ObjectID(req.params.user_id);
	broker.call("offers.get", {"user_id": u_id})
		.then(respond => {
			res.render("index", {
				acceptedOffers: respond
			});
		});
});

app.get("/offer/accept/:user_id/:offer_id", function(req, res) {
	var u_id = req.params.user_id;
	var o_id = req.params.offer_id;
	var o_id2 = req.params.offer_id;
	console.log("o_id: "+o_id);
	console.log("u_id: "+u_id)
	if (Form.findOne({$and: [{"_id": o_id}, {"toUser": u_id}]})) {
		res.render("index", {
			offerAlreadyAccepted: true
		});
	} else {
		broker.call("accept.do", {
			user_id: u_id,
			offer_id: o_id
		})
			.then(respond => res.render("index", {
				offerSuccessfullyAccepted: true
			}));
	}
});

//Logout
app.get('/logout', function (req, res) {
	req.logout();
	req.flash('success_msg', "You have benn logget out!");
	res.redirect('/');
});

//Forgot password - generate token
app.post('/forgot/generateToken', function (req, res, next) {
	console.log(req.flash);
	if (User.findOne({email: req.body.forgotPasswordEmail})) {
		broker.call("reset.generateToken", { email: req.body.forgotPasswordEmail, flash: req.flash });
		res.render('./index', {
			sendForgottenToken: true
		});
	} else {
		res.render('index', {
			emailNonExistant: true
		});
	}
});

//Forgot password - check token
app.get('/reset/:token', function (req, res) {
	User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
		if (!user) {
			req.flash('error', 'Password reset token is invalid or has expired.');
			return res.redirect('/');
		}
		res.render('./index', {
			user: req.user
		});
	});
});

//Forgot password - change password
app.post('/reset/:token', function (req, res) {
	var password = req.body.password;
	var password2 = req.body.confirmPassword;
	var token = req.params.token;
	var flash = req.flash;

	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('password', 'Passwords do not match').equals(password2);

	var errors = req.validationErrors();

	if (errors) {
		console.log(errors);
		console.log("Rozne hasla");
		res.render('./reset/:token', {
			errors: errors
		});
	} else {
		broker.call("reset.newPassword", { password: password, token: token, flash: flash }).then(respond => {
			req.user = respond;
			console.log(req.user);
			res.redirect('/');
		});
		res.redirect('/');
	}
});

//Account activation
app.get('/activate/:token', function (req, res) {
	var token = req.params.token;

	broker.call("register.activate", { token: token }).then(respond => res.render('index', {
		successfulActivation: true
	}));
});

//Delete account
app.post('/delete', function (req, res) {
	console.log(req.user.email);
	broker.call("delete.prepare", {email: req.user.email}).then(respond => res.redirect('/'));
});

//Delete verification
app.get('/delete/:token', function (req, res) {
	var token = req.params.token;

	broker.call("delete.confirm", { token: token }).then(respond => res.render('index', {
		accountDeleted: true
	}));
});

//Edit acccount information
app.post('/edit/account', function (req, res) {

	var firstName = req.body.firstName;
	if(!firstName) firstName = req.user.firstName;
	var lastName = req.body.lastName;
	if(!lastName) lastName = req.user.lastName;
	var companyName = req.body.companyName;
	if(!companyName) companyName = req.user.companyName;
	var companyLocation;
	if (req.user.companyName || companyName) {
		companyLocation = req.body.companyLocation;
		if(!companyLocation) companyLocation = req.user.companyLocation;
	}

	broker.call("edit.user", { email: req.user.email, firstName: firstName, lastName: lastName, companyName: companyName, companyLocation: companyLocation }).then(respond => res.render('index', {
		accountEdited: true
	}));
});

//Set Express listening port on 3000
app.listen(3000);

//Start server
broker.start();