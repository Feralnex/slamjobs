var mongoose = require('mongoose');

// Form Schema
var FormSchema = mongoose.Schema({
    offerCategory: {
        type: String
    },
    companyName: {
        type: String
    },
    location: {
        type: String
    },
    minSalary: {
        type: Number
    },
    maxSalary: {
        type: Number
    },
    description: {
        type: String
    },
    language: {
        type: String
    },
    technology: {
        type: String
    },
    date: {
        type: Date
    },
    userEmail: {
        type: String
    },
    isWork: {
        type: Boolean
    },
    toUser: {
        type: String
    }
}, { collection: "Form" });

var Offer = module.exports = mongoose.model('Form', FormSchema);

module.exports.getOfferById = function(id, callback) {
    var query = {id: id};
    Offer.findOne(query, callback);
};

module.exports.getOffersByKeywordAndLocation = function(keyword, location, callback) {
    var query = {$and: [{offerCategory: {$regex: ".*"+keyword+".*"}}, {location: {$regex: ".*"+location+".*"}}]};
    Offer.find(query, callback);
};

module.exports.getAllOffers = function(callback) {
    var query = {};
    Offer.find(query, callback);
};

module.exports.insertOffer = function(newOffer, callback) {
    newOffer.save(callback);
};
