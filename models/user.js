var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// User Schema
var UserSchema = mongoose.Schema({
	firstName: {
		type: String	
	},
	lastName: {
		type: String	
	},
	password: {
		type: String
	},
	email: {
		type: String,
		index:true
	},
	companyName: {
		type: String,
		index:true,
		default: ""
	},
	companyLocation: {
		type: String,
		index:true,
		default: ""
	},
	resetPasswordToken: {
		type: String
	},
	resetPasswordExpires: {
		type: Date
	},
	accountVerificationToken: {
		type: String
	},
	deleteVerificationToken: {
		type: String
	},
	active: {
		type: Boolean,
		default: false
	}
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.createUser = function(newUser, callback){
	bcrypt.genSalt(10, function(err, salt) {
	    bcrypt.hash(newUser.password, salt, function(err, hash) {
	        newUser.password = hash;
	        newUser.save(callback);
	    });
	});
}

module.exports.getUserByemail = function(email, callback){
	var query = {email: email};
	User.findOne(query, callback);
}

module.exports.getUserByName = function(email, callback){
	var query = {username: username};
	User.findOne(query, callback);
}

module.exports.getUserById = function(id, callback){
	User.findById(id, callback);
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
	bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
    	if(err) throw err;
    	callback(null, isMatch);
	});
}