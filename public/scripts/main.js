$(document).ready(function () {
    setOnClickEvents();
});

function setOnClickEvents() {
    $("#registerButton").click(registerForm);
    $("#signInButton").click(signIn);
    $("#asUserButton").click(asUser);
    $("#asCompanyButton").click(asCompany);
    $("#cancelRegister").click(cancelRegister);
    $("#workOrders").click(workOrders);
    $("#jobOffers").click(jobOffers);
    $("#viewAccountInfo").click(viewInfo);
    $("#offerName").on("input", searchOffers);
    $("#location").on("input", searchOffers);
    $("#forgot").click(generateForgotPassword);
    $("#deleteAccount").click(generateConfirmationPanel);
    $('#edit').click(generateEditAccountInformation);
}

var searchWorkOrdersButton = false;
var searchJobOffersButton = false;

//Show/hide register form with "As user" and "As company" buttons
function registerForm() {
    var register = $("#register");
    var registerForm = $("#registerForm");
    if (register.css("display") === "none") {
        register.css("display", "block");
    } else {
        register.css("display", "none");
        registerForm.css("display", "none");
    }
}

//Create POST request to the web service
function signIn() {
    //POST method to sign in
}

//Show/hide register form with the necessary fields, but without company fields
function asUser() {
    var registerForm = $("#registerForm");
    var companyFields = $(".companyFields");
    if (registerForm.css("display") === "none" || companyFields.css("display") === "block") {
        registerForm.css("display", "block");
        companyFields.css("display", "none");
    } else {
        registerForm.css("display", "none");
    }
}

//Show/hide register form with the necessary fields
function asCompany() {
    var registerForm = $("#registerForm");
    var companyFields = $(".companyFields");
    if (registerForm.css("display") === "none" || companyFields.css("display") === "none") {
        registerForm.css("display", "block");
        companyFields.css("display", "block");
    } else {
        registerForm.css("display", "none");
    }
}

//Show/hide "Search" and "Close" buttons after clicking "Work orders"
function workOrders() {
    var submitSearch = $("#submitSearch");
    if (submitSearch.css("display") === "none" || searchJobOffersButton === true) {
        submitSearch.css("display", "block");
        searchJobOffersButton = false;
        searchWorkOrdersButton = true;

    } else {
        submitSearch.css("display", "none");
        searchWorkOrdersButton = false;
    }
}

// Perform searching offers based on defined criteria
function searchOffers() {
    var keyword = $("#offerName").val();
    var location = $("#location").val();
    $.getJSON("api/offers/get/?keyword=" + keyword + "&location=" + location)
        .done(function (data) {
            var result = "<div class=\"divTable\"><div class=\"divTableBody\">";
            $.each(data, function (key, val) {
                var id = val._id;
                var offerCategory = val.offerCategory;
                var company = val.companyName;
                var location = val.location;
                result = result + "<div class=\"divTableRow\"><a href=\"http://localhost:3000/offer/"+id+"\">";
                result = result + "<div class=\"divTableCell\">" + offerCategory + " @ " + company + "</div>";
                result = result + "<div class=\"divTableCell\">" + location + "</div></a></div>"
            });
            result = result + "</div></div>";
            $("section").html(result);
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request failed: " + err);
        });
}

//Show/hide "Search" and "Close" buttons after clicking "Job offers"
function jobOffers() {
    var submitSearch = $("#submitSearch");
    if (submitSearch.css("display") === "none" || searchJobOffersButton === false) {
        submitSearch.css("display", "block");
        searchWorkOrdersButton = false;
        searchJobOffersButton = true;
    } else {
        submitSearch.css("display", "none");
        searchJobOffersButton = false
    }
}

// Show single offer by ID
function showOffer(id) {
    $.getJSON("api/offers/get/?id=" + id)
        .done(function (data) {
            var result = "<h1>" + data.offerCategory + "</h1><br>";
            result = result + "<b>" + data.companyName + "</b>";
            result = result + "<br>";
            result = result + "<b>Location: </b>" + data.location + "<br>";
            result = result + "<b>Salary: </b> " + data.minSalary + "-" + data.maxSalary + " PLN/month<br>";
            result = result + "<b>Description: </b>" + data.description + "<br>";
            result = result + "<b>Language: </b>" + data.language + "<br>";
            result = result + "<b>Technology: </b>" + data.technology + "<br>";
            result = result + "<b>Added: </b>" + data.date + "<br>";
            result = result + "<br><a href=\"javascript:getOffers();\" class=\"goback\">Go back</a>";
            $("section").html(result);
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request failed: " + err);
        });
}


// Get all offers from DB and display them on the website
function getOffers() {
    $.getJSON("api/offers/get")
        .done(function (data) {
            var result = "<div class=\"divTable\"><div class=\"divTableBody\">";
            $.each(data, function (key, val) {
                var id = val._id;
                var offerCategory = val.offerCategory;
                var company = val.companyName;
                var location = val.location;
                result = result + "<div class=\"divTableRow\"><a href=\"http://localhost:3000/offer/"+id+"\">";
                result = result + "<div class=\"divTableCell\">" + offerCategory + " @ " + company + "</div>";
                result = result + "<div class=\"divTableCell\">" + location + "</div></a></div>"
            });
            result = result + "</div></div>";
            $("section").html(result);
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request failed: " + err);
        });
}

//Hide register form after clicking "Cancel" button in register panel
function cancelRegister() {
    var register = $("#register");
    var registerForm = $("#registerForm");
    registerForm.css("display", "none");
    register.css("display", "none");
}

//View account information
var angle = 0
function viewInfo() {
    angle = (angle + 180) % 360;
    var className = "rotate" + angle;
    $("#viewAccountInfo").attr('class', className);
    var accountInfo = $("#accountInfo");
    if (accountInfo.css("display") === "none" && angle == 180) {
        accountInfo.css("display", "block");
    } else {
        accountInfo.css("display", "none");
    }
}

//Forgot password view
function generateForgotPassword() {
    var result = "<h3>Reset password</h3>";
    result = result + "<form id=\"forgotForm\" action=\"/forgot/generateToken\" method=\"POST\">";
    result = result + "<input id=\"forgotPasswordEmail\" class=\"form formOb\"text\" name=\"forgotPasswordEmail\" placeholder=\"E-Mail...\" />";
    result = result + "<button type=\"submit\" id=\"submitForgot\">Submit</button>";
    result = result + "</form>";
    $("section").html(result);
}

//Reset password view
function generateResetPassword() {
    var result = "<h3>Reset Password</h3>";
    result = result + "<p>Enter a new password:";
    result = result + "<div id=\"reset\"><form id=\"resetForm\" method=\"POST\">";
    result = result + "<input id=\"newPassord\" class=\"form leftSideForm\" type=\"password\" name=\"password\" placeholder=\"Password\" />";
    result = result + "<input id=\"newPassword2\" class=\"form rightSideForm\" type=\"password\" name=\"confirmPassword\" placeholder=\"Confirm password\" />";
    result = result + "<button type=\"submit\" id=\"submitReset\">Submit</button>";
    result = result + "</form></div>";
    $("section").html(result);
}

//Delete account confirmation view
function generateConfirmationPanel() {
    var result = "<h3>Delete account</h3>";
    result = result + "<p>Are You sure you want to delete this account?";
    result = result + "<form id=\"deleteForm\" action=\"/delete\" method=\"POST\">";
    result = result + "<button type=\"submit\" id=\"submitDelete\">Confirm</button>";
    result = result + "</form>";
    $("section").html(result);
}

//Edit account information Panel
function generateEditAccountInformation() {
    var result = "<h3>Edit account</h3>";
    result = result + "<p>Enter new account information that You want to change";
    result = result + "<form id=\"editForm\" action=\"/edit/account\" method=\"POST\">";
    result = result + "<input class=\"form leftSideForm formObjects\" type=\"text\" name=\"firstName\" placeholder=\"First name\" />";
    result = result + "<input class=\"form rightSideForm formObjects\" type=\"text\" name=\"lastName\" placeholder=\"Last name\" />";
    result = result + "<input class=\"form leftSideForm formObjects\" type=\"text\" name=\"companyName\" placeholder=\"Company name\" />";
    result = result + "<input class=\"form rightSideForm formObjects\" type=\"text\" name=\"companyLocation\" placeholder=\"Company location\" />";
    result = result + "<button type=\"submit\" id=\"submitEdit\">Confirm</button>";
    result = result + "</form>";
    $("section").html(result);
}